
#!/bin/sh

sudo dnf update -y && sudo dnf upgrade -y
sudo dnf install leonidas-backgrounds-lion -y
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
sudo dnf -y install clamav clamav-update -y
sudo freshclam
sudo dnf install   https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm -y
sudo dnf install   https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm -y
sudo dnf group update core -y
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remote-add --if-not-exists fedora oci+https://registry.fedoraproject.org
flatpak install flathub org.gnome.Polari -y
sudo dnf install snapd -y
sudo snap install snap-store -y
sudo snap install snap-storesudo dnf groupupdate sound-and-video -y
sudo dnf groupupdate vagrant -y
sudo dnf groupupdate admin-tools -y
sudo dnf groupupdate audio -y
sudo dnf install obs -y
sudo dnf groupupdate c-development -y
sudo dnf groupupdate cloud-infrastructure -y
sudo dnf groupupdate cloud-server -y
sudo dnf install fedora-workstation-repositories -y
sudo dnf config-manager --set-enabled google-chrome
sudo dnf install google-chrome-stable -y
sudo dnf install dnf-automatic -y
env EDITOR='nano -w' sudoedit /etc/dnf/automatic.conf
systemctl enable --now dnf-automatic.timer
sudo dnf install akmod-nvidia -y
sudo dnf -y install ffmpeg -y
sudo dnf -y install ffmpeg-devel -y
sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin -y
sudo snap install bpytop -y
sudo dnf install openssh-server -y
sudo dnf install VirtualBox -y
sudo dnf -y install dnf-plugins-core -y
sudo dnf config-manager     --add-repo     https://download.docker.com/linux/fedora/docker-ce.repo -y
sudo dnf config-manager --set-enabled docker-ce-nightly -y
sudo dnf install docker-ce docker-ce-cli containerd.io -y
sudo docker run -d --name node_exporter  --net="host"   --pid="host"   --restart=always -v "/:/host:ro,rslave"   quay.io/prometheus/node-exporter:latest   --path.rootfs=/host
